import { Component } from '../component/component';

export interface IView {
    open(root: HTMLElement, data: any): void;
    close(): void;
}

export class View  implements IView {
    protected root: HTMLElement = document.createElement('div');
    protected components: Component[] = [];

    public open(data: any = {}): void {
        this.root = data.root;
    }

    public close(): void {
        if (this.components) {
            this.components.forEach((component: any) => {
                component.delete();
            });
        }
    }
}

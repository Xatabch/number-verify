import { Model } from '../model/model';
import { View } from '../view/view';

export interface IController {
    open(): void;
    close(): void;
}

export class Controller implements IController {
    protected model: Model;
    protected view: View;

    constructor(model: Model, view: View) {
        this.model = model;
        this.view = view;

        this.render = this.render.bind(this); // ??????????
    }

    public open(data: any = {}): void {
        this.model.init({root: data});
    }

    public close(): void {
        this.view.close();
    }

    protected render(data: any = {}): void {
        this.view.open(data);
    }
}

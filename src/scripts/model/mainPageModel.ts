import EventEmitterMixin from '../mixin/eventEmitterMixin';
import { INIT_EVENT, RESPONSE_VERIFY_PHONE_EVENT } from '../modules/events';
import { Model } from './model';

export default class MainModel extends EventEmitterMixin(Model) {
    constructor() {
        super();
        this.verifyNumbers = this.verifyNumbers.bind(this);
    }
    public init(data: any = {}): void {
        this.emit(INIT_EVENT, {root: data.root, isRender: true});
    }

    public verifyNumbers(numbers: string): void {
        // Here you can validate your numbers that user input
        // For example '22' is incorrect
        if (numbers !== '44' || !numbers) {
            this.emit(RESPONSE_VERIFY_PHONE_EVENT, {error: true, isRender: false});
        } else {
            this.emit(RESPONSE_VERIFY_PHONE_EVENT, {error: false, isRender: false});
        }
    }
}


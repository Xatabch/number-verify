import '../../style/style.css';
import { Component } from './component';

export default class PhoneVerifyComponent extends Component {
    /** The function in which the string will be transferred from the entered numbers */
    protected callback: any;
    /** HTML element to which the calendar will be attached */
    protected root: HTMLElement = document.createElement('div');
    /** Mask on which the item is created */
    private mask: string;
    /** Regular expressions for searching Russian and English characters */
    private errorRegexp: RegExp = /[a-hj-wy-zа-яё]/gi;
    /** Regular expression to replace the mask for HTML elements */
    private replaceRegexp: RegExp = /[\d\-\I\*()+\x]/gi;

    constructor(callback: any = (): any => null, mask: string = '') {
        super(callback);
        this.mask = mask;
        this.onChangeInput = this.onChangeInput.bind(this);
    }

    /** Method creating component */
    public create(root: HTMLElement, data: any = {}): void {
        super.create(root, data);
        this.render(data);
        this.createEventListeners();
    }

    /** Method remove component */
    public delete(): void {
        this.removeEventListener();
        const form: HTMLElement = this.root.querySelector('.phone-verify') as HTMLElement;
        this.root.removeChild(form);
    }

    /** The method takes the component to the error state */
    public renderError(): void {
        const errorMessage: HTMLElement = this.root.querySelector('.phone-verify__error-mes') as HTMLElement;
        const phoneInput: NodeListOf<Element> = this.root.
                                                querySelectorAll(`.phone-verify_theme_active`) as NodeListOf<Element>;
        errorMessage.classList.remove('phone-verify__error-mes_theme_none');

        phoneInput.forEach((input: Element) => {
            input.classList.add('phone-verify__element_input_theme_error');
        });
    }

    /** The method takes the component to its normal state */
    public hideError(): void {
        const errorMessage: HTMLElement = this.root.
                                          querySelector('.phone-verify__error-mes') as HTMLElement;
        const phoneInput: NodeListOf<Element> = this.root.
                                                querySelectorAll('.phone-verify_theme_active') as NodeListOf<Element>;
        errorMessage.classList.add('phone-verify__error-mes_theme_none');

        phoneInput.forEach((input: Element) => {
            input.classList.remove('phone-verify__element_input_theme_error');
        });
    }

    /** The method of pinning the component to the HTML element */
    private render(data: any = {}): void {
        const form: HTMLElement = document.createElement('div');
        form.classList.add('phone-verify');

        if (!this.errorRegexp.test(this.mask) && this.mask) {
            form.innerHTML = this.replaceMask();

            const errorMessage: HTMLElement = document.createElement('div') as HTMLElement;
            errorMessage.classList.add('phone-verify__error-mes');
            errorMessage.classList.add('phone-verify__error-mes_theme_none');
            errorMessage.textContent = 'Неверный номер, попробуйте еще раз';

            form.appendChild(errorMessage);
        }

        this.root.appendChild(form);
    }

    /** The method replaces the mask on the HTML components using a regular expression */
    private replaceMask(): string {
        return this.mask.replace(this.replaceRegexp, (str: string): string => {
            if (str === 'I') {
                return `<input type='text'
                 class='phone-verify__element_input phone-verify_theme_active' placeholder='_' maxlength='1'>`;
            } else if (str === 'X') {
                return `<input type='text'
                         class='phone-verify__element_input phone-verify_theme_disable' value='X' disabled>`;
            } else if (str === '*') {
                return `<input type='text'
                         class='phone-verify__element_input phone-verify_theme_disable' value='●' disabled>`;
            } else if (/[0-9]/.test(str)) {
                return `<input type='text'
                         class='phone-verify__element_input phone-verify_theme_disable' value='${str}' disabled>`;
            } else {
                return `<div class='phone-verify__element_symbol'>${str}</div>`;
            }
        });
    }

    /** The method initializes all event listeners for the component. */
    private createEventListeners(): void {
        this.createOnChangeInputListener();
    }

    /** Method that removes all event listeners from a component */
    private removeEventListener(): void {
        this.removeOnChangeInputListener();
    }

    /** Method responsible for handling input form changes */
    private onChangeInput(event: any): void {
        const activeInputs = this.root.querySelectorAll('.phone-verify_theme_active');
        let inputs: string = '';
        activeInputs.forEach((input: Element): void => {
            inputs += (input as HTMLFormElement).value;
        });

        this.callback(inputs);
    }

    /** The method initializes listener for method that responsible for handling input form changes */
    private createOnChangeInputListener(): void {
        const activeInputs = this.root.querySelectorAll('.phone-verify_theme_active');

        activeInputs.forEach((input: Element): void => {
            input.addEventListener('change', this.onChangeInput, true);
        });
    }

    /** The method remove listener for method that responsible for handling input form changes */
    private removeOnChangeInputListener(): void {
        const activeInputs = this.root.querySelectorAll('.phone-verify_theme_active');

        activeInputs.forEach((input: Element): void => {
            input.removeEventListener('change', this.onChangeInput, true);
        });
    }
}

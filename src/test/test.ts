import 'ignore-styles';
import PhoneVerifyComponent from '../scripts/component/phoneVerifyComponent';
let jsdom = require('mocha-jsdom');
var assert = require('assert');

describe('PhoneVerify', function() {
  jsdom({
    url: "http://localhost"
  });

  describe('Тестирование HTML', function() {
    it('Созданный HTML код должен соответствовать поданной маске', function() {
      let div = document.createElement('div');
      let mask = `+7(985)0II-**-**`;
      let phoneVerify = new PhoneVerifyComponent((): void => {}, mask);
      phoneVerify.create(div);

      assert.equal(div.innerHTML, '<div class="phone-verify"><div class="phone-verify__element_symbol">+</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="7" disabled=""><div class="phone-verify__element_symbol">(</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="9" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="8" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="5" disabled=""><div class="phone-verify__element_symbol">)</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="0" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_active" placeholder="_" maxlength="1"><input type="text" class="phone-verify__element_input phone-verify_theme_active" placeholder="_" maxlength="1"><div class="phone-verify__element_symbol">-</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="●" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="●" disabled=""><div class="phone-verify__element_symbol">-</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="●" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="●" disabled=""><div class="phone-verify__error-mes phone-verify__error-mes_theme_none">Неверный номер, попробуйте еще раз</div></div>');
    });
    it('Если в маске присутствуют буквы русского или английского алфавита(кроме спец символов), ничего не отображается', function() {
      let div = document.createElement('div');
      let mask = `+7(985)0IприветI-**-**`;
      let phoneVerify = new PhoneVerifyComponent((): void => {}, mask);
      phoneVerify.create(div);

      assert.equal(div.innerHTML, '<div class="phone-verify"></div>');
    });
    it('Созданный HTML код должен соответствовать поданной маске(спец символ X)', function() {
      let div = document.createElement('div');
      let mask = `+7(985)0II-XX-XX`;
      let phoneVerify = new PhoneVerifyComponent((): void => {}, mask);
      phoneVerify.create(div);

      assert.equal(div.innerHTML, '<div class="phone-verify"><div class="phone-verify__element_symbol">+</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="7" disabled=""><div class="phone-verify__element_symbol">(</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="9" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="8" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="5" disabled=""><div class="phone-verify__element_symbol">)</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="0" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_active" placeholder="_" maxlength="1"><input type="text" class="phone-verify__element_input phone-verify_theme_active" placeholder="_" maxlength="1"><div class="phone-verify__element_symbol">-</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><div class="phone-verify__element_symbol">-</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><div class="phone-verify__error-mes phone-verify__error-mes_theme_none">Неверный номер, попробуйте еще раз</div></div>');
    });
    it('Созданный HTML код в состоянии ошибки', function() {
      let div = document.createElement('div');
      let mask = `+7(985)0II-XX-XX`;
      let phoneVerify = new PhoneVerifyComponent((): void => {}, mask);
      phoneVerify.create(div);
      phoneVerify.renderError();

      assert.equal(div.innerHTML, '<div class="phone-verify"><div class="phone-verify__element_symbol">+</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="7" disabled=""><div class="phone-verify__element_symbol">(</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="9" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="8" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="5" disabled=""><div class="phone-verify__element_symbol">)</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="0" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_active phone-verify__element_input_theme_error" placeholder="_" maxlength="1"><input type="text" class="phone-verify__element_input phone-verify_theme_active phone-verify__element_input_theme_error" placeholder="_" maxlength="1"><div class="phone-verify__element_symbol">-</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><div class="phone-verify__element_symbol">-</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><div class="phone-verify__error-mes">Неверный номер, попробуйте еще раз</div></div>');
    });
    it('Созданный HTML код переход из состоянии ошибки в состояние без ошибки', function() {
      let div = document.createElement('div');
      let mask = `+7(985)0II-XX-XX`;
      let phoneVerify = new PhoneVerifyComponent((): void => {}, mask);
      phoneVerify.create(div);
      phoneVerify.renderError();
      phoneVerify.hideError();

      assert.equal(div.innerHTML, '<div class="phone-verify"><div class="phone-verify__element_symbol">+</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="7" disabled=""><div class="phone-verify__element_symbol">(</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="9" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="8" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="5" disabled=""><div class="phone-verify__element_symbol">)</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="0" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_active" placeholder="_" maxlength="1"><input type="text" class="phone-verify__element_input phone-verify_theme_active" placeholder="_" maxlength="1"><div class="phone-verify__element_symbol">-</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><div class="phone-verify__element_symbol">-</div><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><input type="text" class="phone-verify__element_input phone-verify_theme_disable" value="X" disabled=""><div class="phone-verify__error-mes phone-verify__error-mes_theme_none">Неверный номер, попробуйте еще раз</div></div>');
    });
    it('Созданный HTML код при пустой маске должен быть пустым div-ом', function() {
      let div = document.createElement('div');
      let mask = ``;
      let phoneVerify = new PhoneVerifyComponent((): void => {}, mask);
      phoneVerify.create(div);

      assert.equal(div.innerHTML, '<div class="phone-verify"></div>');
    });
    it('div должен остаться пустым, после удаления элемента', function() {
      let div = document.createElement('div');
      let mask = `+7(985)0II-XX-XX`;
      let phoneVerify = new PhoneVerifyComponent((): void => {}, mask);
      phoneVerify.create(div);
      phoneVerify.delete();

      assert.equal(div.innerHTML, '');
    });
  });
});
import { Component } from '../component/component';
import PhoneVerifyComponent from '../component/phoneVerifyComponent';
import EventEmitterMixin from '../mixin/eventEmitterMixin';
import { VERIFY_PHONE_EVENT } from '../modules/events';
import { View } from './view';

export default class MainView extends EventEmitterMixin(View) {
    protected root: HTMLElement = document.createElement('div');
    protected components: Component[] = [];
    private phoneVerifyComponent: PhoneVerifyComponent = new PhoneVerifyComponent();
    private inputNumbers: string = '';

    constructor() {
        super();
        this.render = this.render.bind(this);
    }

    public open(data: any = {}): void {
        super.open(data);
        this.onChangePhone = this.onChangePhone.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);

        const mask = `+7(985)0II-**-**`;
        this.phoneVerifyComponent = new PhoneVerifyComponent(this.onChangePhone, mask);
        this.components.push(this.phoneVerifyComponent);

        this.render(data);

        this.createEventListeners();
    }

    public close(): void {
        this.removeEventListeners();
    }

    public render(data: any): void {
        if (data.isRender) {
            data.root.innerHTML = '';
            this.renderForm();
            this.renderPhoneVerify();
            this.renderButton();
        } else {
            if (data.error) {
                this.phoneVerifyComponent.renderError();
            } else {
                this.phoneVerifyComponent.hideError();
            }
        }
    }

    private renderPhoneVerify(): void {
        const formBlock: HTMLElement = this.root.querySelector('.phone-form') as HTMLElement;
        this.phoneVerifyComponent.create(formBlock);
    }

    private renderForm(): void {
        const formBlock: HTMLElement = document.createElement('form') as HTMLElement;
        formBlock.classList.add('phone-form');

        this.root.appendChild(formBlock);
    }

    private renderButton(): void {
        const formBlock: HTMLElement = this.root.querySelector('.phone-form') as HTMLElement;

        const submitBlock: HTMLInputElement = document.createElement('input') as HTMLInputElement;
        submitBlock.classList.add('phone-submit');
        submitBlock.type = 'submit';
        submitBlock.value = 'submit';

        formBlock.appendChild(submitBlock);
    }

    private onChangePhone(inputs: string = '') {
        this.inputNumbers = inputs;
    }

    private onFormSubmit(event: any): void {
        event.preventDefault();
        this.emit(VERIFY_PHONE_EVENT, this.inputNumbers);
    }

    private createOnFormSubmitListener(): void {
        const formBlock: HTMLFormElement = this.root.querySelector('.phone-form') as HTMLFormElement;
        formBlock.addEventListener('submit', this.onFormSubmit, true);
    }

    private removeOnFormSubmitListener(): void {
        const formBlock: HTMLFormElement = this.root.querySelector('.phone-form') as HTMLFormElement;
        formBlock.removeEventListener('submit', this.onFormSubmit, true);
    }

    private createEventListeners(): void {
        this.createOnFormSubmitListener();
    }

    private removeEventListeners(): void {
        this.removeOnFormSubmitListener();
    }
}

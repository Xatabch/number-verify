import '../style/style.css';
import MainController from './controller/mainPageController';
import MainModel from './model/mainPageModel';
import MainView from './view/mainPageView';

const mainView = new MainView();
const mainModel = new MainModel();
const mainController = new MainController(mainModel, mainView);

const root = document.querySelector('#application');
mainController.open(root);

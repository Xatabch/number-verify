/* General events */

export const INIT_EVENT: string = 'init';
export const INIT_ERROR_EVENT: string = 'init_error';

/* Phone verify events */

export const VERIFY_PHONE_EVENT: string = 'verify_phone_event';
export const RESPONSE_VERIFY_PHONE_EVENT: string = 'response_verify_phone_event';

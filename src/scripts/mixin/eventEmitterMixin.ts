import { Constructor } from '../modules/helpers';

export default function EventEmitterMixin<TBase extends Constructor>(base: TBase) {
    class EventEmitter extends base {
        private events: any[] = [];

        public on(event: any = -1, listener: any = {}): void {
            (this.events[event] || (this.events[event] = [])).push(listener);
        }

        public remove(event: any = -1): void {
            delete this.events[event];
        }

        public emit(event: any = -1, arg: any = {}): void {
            (this.events[event] || []).forEach((listener: any) => listener(arg));
        }
    }

    return EventEmitter;
}

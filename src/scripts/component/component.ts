export interface IComponent {
    create(root: HTMLElement, data: any): void;
}

export class Component implements IComponent {
    protected root: HTMLElement = document.createElement('div');
    protected callback: any;

    constructor(callback: any) {
        this.callback = callback;
    }

    public create(root: HTMLElement, data: any = {}): void {
        this.root = root;
    }
}

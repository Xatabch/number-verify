import MainModel from '../model/mainPageModel';
import MainView from '../view/mainPageView';
import { Controller } from './controller';

import { INIT_EVENT, RESPONSE_VERIFY_PHONE_EVENT, VERIFY_PHONE_EVENT } from '../modules/events';

export default class MainController extends Controller {
    protected model: MainModel = new MainModel();
    protected view: MainView = new MainView();

    constructor(model: MainModel, view: MainView) {
        super(model, view);

        this.model.on(INIT_EVENT, this.render);
        this.view.on(VERIFY_PHONE_EVENT, this.model.verifyNumbers);
        this.model.on(RESPONSE_VERIFY_PHONE_EVENT, this.view.render);
    }
}

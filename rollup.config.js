// Plugins
import babel from 'rollup-plugin-babel';
import typescript from 'rollup-plugin-typescript2';
import html from 'rollup-plugin-bundle-html';
import css from 'rollup-plugin-css-only';
import tslint from "rollup-plugin-tslint";
import postcss from 'rollup-plugin-postcss';
import pkg from './package.json';

export default [{
    input: 'src/scripts/main.ts',
    output: {
        file: 'build/js/main.js',
        format: 'es',
    },
    plugins: [ 
        babel(),
        tslint({
            include: [
                'src/scripts/**/*.ts',
            ],
            exclude: [
                '*.css'
            ]
        }),
        typescript({
            typescript: require('typescript'),
            tsconfig: "tsconfig.json",
        }), 
        html({
            template: 'src/index.html',
            dest: 'build/',
            filename: 'index.html',
            inject: 'body',
        }),
        postcss({
            minimize: true,
            extract: './build/css/style.css',
        }),
        css({ 
            output: 'build/css/style.css' 
        }),
    ]
  },
{
    input: 'src/scripts/component/phoneVerifyComponent.ts', 
    output: [{
        file: pkg.main,
        format: 'cjs',
      },
      {
        file: pkg.module,
        format: 'es',
      }],
    plugins: [ 
        babel(),
        typescript({
            typescript: require('typescript'),
            tsconfig: "tsconfig.component.json",
            abortOnError: true,
        }),
        postcss({
            minimize: true,
            extract: 'component/css/phoneVerifyComponentStyle.css'
        }),
        css({ 
            output: 'component/css/phoneVerifyComponentStyle.css' 
        })
    ]
}];
